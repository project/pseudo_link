<?php

namespace Drupal\pseudo_link\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Microsoft Exchange Group Notification.
 */
class EntitySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pseudo_link.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pseudo_link_entity.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pseudo_link.config');

    $form['information'] = [
      '#type' => 'vertical_tabs',
    ];
    $entitty_bundles = pseudo_link_get_entitty_bundles();
    $bundle_data = [];

    foreach ($entitty_bundles as $key => $entitty_bundle) {
      $entity_machine_name = $entitty_bundle['entity_key'];
      $entity_label = $entitty_bundle['entity_label'];
      $form[$entity_machine_name] = [
        '#type' => 'details',
        '#title' => $this->t('@label', ['@label' => $entity_label]),
        '#group' => 'information',
      ];

      $bundles = $entitty_bundle['bundles'];

      $form[$entity_machine_name][$key . '_tab'] = [
        '#type' => 'vertical_tabs',
      ];

      foreach ($bundles as $bkey => $bundle) {
        $bkey = $bundle['bundle_key'];
        $label = $bundle['bundle_label'];
        $entity_bundle = $entity_machine_name . '_' . $bkey;
        $bundle_data[$entity_machine_name][] = $bkey;
        if (is_object($label)) {
          $label = $label->getUntranslatedString();
        }

        $form[$entity_machine_name][$bkey] = [
          '#type' => 'details',
          '#title' => $this->t('@label', ['@label' => $label]),
          '#group' => $key . '_tab',
        ];

        $form[$entity_machine_name][$bkey][$entity_bundle . '_enable_link'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable Pseudo Link'),
          '#default_value' => $config->get($entity_bundle . '_enable_link') ?? '0',
          '#description' => $this->t('Enable pseudo link for %label', ['%label' => $label]),
        ];

        $form[$entity_machine_name][$bkey][$entity_bundle . '_link_prefix'] = [
          '#type' => 'textfield',
          '#title' => $this
            ->t('Link Prefix'),
          '#default_value' => $config->get($entity_bundle . '_link_prefix') ?? '',
          '#description' => $this->t('Pseudo link prefix for %label. <br>Eg, &lt;div class=&quot;link-wrapper&quot;&gt;&lt;/div&gt;, &lt;i class=&quot;fa fa-book&quot;&gt;&lt;/i&gt;, &lt;span&gt;&lt;/span&gt; etc.', ['%label' => $label]),
          '#states' => [
            'visible' => [
              ':input[name="' . $entity_bundle . '_enable_link"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form[$entity_machine_name][$bkey][$entity_bundle . '_link_suffix'] = [
          '#type' => 'textfield',
          '#title' => $this
            ->t('Link Suffix'),
          '#default_value' => $config->get($entity_bundle . '_link_suffix') ?? '',
          '#description' => $this->t('Pseudo link suffix for %label.<br>Eg. &lt;/div&gt;, &lt;/span&gt; etc.', ['%label' => $label]),
          '#states' => [
            'visible' => [
              ':input[name="' . $entity_bundle . '_enable_link"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form[$entity_machine_name][$bkey][$entity_bundle . '_link_text'] = [
          '#type' => 'textfield',
          '#title' => $this
            ->t('Link Text'),
          '#default_value' => $config->get($entity_bundle . '_link_text') ?? 'Read More',
          '#description' => $this->t('Pseudo link text for %label', ['%label' => $label]),
          '#states' => [
            'visible' => [
              ':input[name="' . $entity_bundle . '_enable_link"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form[$entity_machine_name][$bkey][$entity_bundle . '_link_class'] = [
          '#type' => 'textfield',
          '#title' => $this
            ->t('Link Class Atribute'),
          '#default_value' => $config->get($entity_bundle . '_link_class') ?? '',
          '#description' => $this->t('Pseudo link class for %label', ['%label' => $label]),
          '#states' => [
            'visible' => [
              ':input[name="' . $entity_bundle . '_enable_link"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form[$entity_machine_name][$bkey][$entity_bundle . '_open_new_tab'] = [
          '#type' => 'checkbox',
          '#title' => $this
            ->t('Open link in a new tab'),
          '#default_value' => $config->get($entity_bundle . '_open_new_tab') ?? '0',
          '#description' => $this->t('Pseudo link open in a new tab.'),
          '#states' => [
            'visible' => [
              ':input[name="' . $entity_bundle . '_enable_link"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $form[$entity_machine_name][$bkey][$entity_bundle . '_info'] = [
          '#type' => 'item',
          '#description' => $this->t('<small>Fulsh the caches if the changes are not reflecting on the manage display and pages.</small>'),
        ];

      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $pseudo_link_get_entitty_bundles = pseudo_link_get_entitty_bundles();
    $config = $this->config('pseudo_link.config');
    foreach ($pseudo_link_get_entitty_bundles as $entitty_bundles) {
      $bundles = $entitty_bundles['bundles'];
      $entity_machine_name = $entitty_bundles['entity_key'];
      if (!empty($bundles)) {
        foreach ($bundles as $bundle) {
          $entity_bundle = $entity_machine_name . '_' . $bundle['bundle_key'];

          $link_enable = $entity_bundle . '_enable_link';
          if (isset($values[$link_enable])) {
            $config->set($link_enable, $values[$link_enable]);
          }

          $link_prefix = $entity_bundle . '_link_prefix';
          if (isset($values[$link_prefix])) {
            $config->set($link_prefix, $values[$link_prefix]);
          }

          $link_suffix = $entity_bundle . '_link_suffix';
          if (isset($values[$link_suffix])) {
            $config->set($link_suffix, $values[$link_suffix]);
          }

          $link_text = $entity_bundle . '_link_text';
          if (isset($values[$link_text])) {
            $config->set($link_text, $values[$link_text]);
          }

          $link_class = $entity_bundle . '_link_class';
          if (isset($values[$link_class])) {
            $config->set($link_class, $values[$link_class]);
          }

          $open_new_tab = $entity_bundle . '_open_new_tab';
          if (isset($values[$open_new_tab])) {
            $config->set($open_new_tab, $values[$open_new_tab]);
          }
        }
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
